package com.ricko18ti.cafeku

import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.fragment_admin_tambah_cafe.view.*
import java.util.*

class AdminTambahCafeFragment : Fragment() {

    private lateinit var namacafe: EditText
    private lateinit var alamatcafe: EditText
    private lateinit var keterangan: EditText
    private lateinit var latitude: EditText
    private lateinit var longitude: EditText
    private lateinit var imgcafe : ImageView
    private lateinit var imguri :Uri
    private lateinit var storage: FirebaseStorage
    private lateinit var storageReference: StorageReference
    private lateinit var randomKey :String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_admin_tambah_cafe, container, false)

        namacafe = view.findViewById(R.id.nama_cafe)
        alamatcafe = view.findViewById(R.id.alamat_cafe)
        keterangan = view.findViewById(R.id.keterangan_cafe)
        latitude = view.findViewById(R.id.koordinat_x)
        longitude = view.findViewById(R.id.koordinat_y)
        imgcafe = view.findViewById(R.id.img_cafe)

        storage = FirebaseStorage.getInstance()
        storageReference = storage.getReference()

        view.btn_upload.setOnClickListener{
            pilihGambarCafe()
        }

        view.btn_simpan.setOnClickListener{
            uploadGambar()
        }
        return view
    }

    private fun pilihGambarCafe() {
        var intent = Intent()
        intent.setType("image/*")
        intent.setAction(Intent.ACTION_GET_CONTENT)
        startActivityForResult(intent, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (data != null) {
            imguri = data.getData()!!
            imgcafe.setImageURI(imguri)
        }
    }

    private fun uploadGambar() {
        val gambar = imgcafe.drawable == null
        if(gambar) {
            Toast.makeText(context, "Gambar belum dipilih", Toast.LENGTH_SHORT).show()
            return
        }
        val pd = ProgressDialog(context)
        pd.setTitle("Upload Gambar")
        pd.show()

        randomKey = UUID.randomUUID().toString()
        val imageRef: StorageReference = storageReference.child("cafe/" + randomKey)

        imageRef.putFile(imguri)
            .addOnSuccessListener { taskSnapshot -> // Get a URL to the uploaded content
                pd.dismiss()
                Toast.makeText(context, "Gambar berhasil diupload", Toast.LENGTH_SHORT) //getActivity() atau getApplicationContext()
                    .show()
                insert()
            }
            .addOnFailureListener {
                pd.dismiss()
                Toast.makeText(context, "Gambar gagal diupload", Toast.LENGTH_SHORT) //getActivity() atau getApplicationContext()
                    .show()
            }
            .addOnProgressListener { taskSnapshot ->
                val persen = (100.0 * taskSnapshot.bytesTransferred) / taskSnapshot.totalByteCount
                pd.setMessage("Progress : " + persen.toInt() + "%")
            }
    }

    private fun insert(){
        var ref: DatabaseReference = FirebaseDatabase.getInstance().getReference("cafe")

        val nama_cafe = namacafe.text.toString().trim()
        val alamat_cafe = alamatcafe.text.toString()
        val keterangan_cafe = keterangan.text.toString()
        val koordinat_x = latitude.text.toString()
        val koordinat_y = longitude.text.toString()

        if (nama_cafe.isEmpty() or alamat_cafe.isEmpty() or keterangan_cafe.isEmpty() or koordinat_x.isEmpty() or koordinat_y.isEmpty()) {
            Toast.makeText(context, "Data tidak boleh kosong", Toast.LENGTH_SHORT) //getActivity() atau getApplicationContext()
                .show()
            return
        }

        val id_cafe = ref.push().key
        val cafe = Cafe(id_cafe!!, nama_cafe, alamat_cafe, randomKey, keterangan_cafe, koordinat_x, koordinat_y)
        ref.child(id_cafe).setValue(cafe).addOnCompleteListener {
            Toast.makeText(context, "Data berhasil ditambahkan", Toast.LENGTH_SHORT) //getActivity() atau getApplicationContext()
                .show()
        }
    }
}