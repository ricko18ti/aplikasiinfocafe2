package com.ricko18ti.cafeku

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.firebase.storage.FirebaseStorage
import java.io.File

class CafeAdapter(
    val CafeContext: Context,
    val layoutResId: Int,
    val cafeList: List<Cafe>
): ArrayAdapter<Cafe>(CafeContext, layoutResId, cafeList) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater: LayoutInflater = LayoutInflater.from(CafeContext)
        val view: View = layoutInflater.inflate(layoutResId, null)

        val namacafe: TextView = view.findViewById(R.id.nama_cafe)
        val gambarcafe :ImageView = view.findViewById(R.id.img_cafe)
        val alamat: TextView = view.findViewById(R.id.alamat_cafe)
        val keterangan: TextView = view.findViewById(R.id.keterangan_cafe)

        //val latitude: TextView = view.findViewById(R.id.koordinat_x)
        //val longtitude: TextView = view.findViewById(R.id.koordinat_y)

        val cafe = cafeList[position]

        namacafe.text = "Nama : " + cafe.nama_cafe
        alamat.text = "Alamat : " + cafe.alamat_cafe
        keterangan.text = "Keterangan : " + cafe.keterangan

        //latitude.text = "Latitude : " + cafe.latitude
        //longtitude.text = "Longtitude : " + cafe.latitude

        var storageReference = FirebaseStorage.getInstance().getReference().child("cafe/"+cafe.gambarcafe)
        var localFile = File.createTempFile(cafe.gambarcafe, "jpeg")
        storageReference.getFile(localFile)
            .addOnSuccessListener { taskSnapshot ->
                var bitmap: Bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath())
                gambarcafe.setImageBitmap(bitmap)
            }
            .addOnFailureListener { exception ->
                Toast.makeText(context, exception.toString(), Toast.LENGTH_SHORT).show()
            }
        return view
    }
}