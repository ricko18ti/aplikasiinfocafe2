package com.ricko18ti.cafeku

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_user_cafe.*
import kotlinx.android.synthetic.main.fragment_user_cafe.view.*

class UserCafeFragment : Fragment() {

    private lateinit var listData: ListView
    private lateinit var ref: DatabaseReference
    private lateinit var cafeList: MutableList<Cafe>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_user_cafe, container, false)
        ref = FirebaseDatabase.getInstance().getReference("cafe")
        listData = view.findViewById(R.id.list_cafe)
        cafeList = mutableListOf()


        ref.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.exists()) {
                    cafeList.clear()
                    for (s in snapshot.children) {
                        val cafe = s.getValue(Cafe::class.java)
                        if (cafe != null) {
                            cafeList.add(cafe)
                        }
                    }

                    val adapter = context?.let {
                        CafeAdapter(
                            it,
                            R.layout.layout_list_cafe, cafeList)
                    }
                    listData.adapter = adapter
                }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })

        view.btn_cari_cafe.setOnClickListener {
            caricafe()
        }

        return view
    }

    private fun caricafe() {
        var query = edt_cari_cafe.text.toString()
        var ref: DatabaseReference = FirebaseDatabase.getInstance().getReference("cafe")
        ref.orderByChild("nama_cafe").startAt(query).endAt(query+"\uf8ff")
            .addValueEventListener(object: ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if(snapshot.exists()) {
                        cafeList.clear()
                        for (s in snapshot.children) {
                            val cafe = s.getValue(Cafe::class.java)
                            if (cafe != null) {
                                cafeList.add(cafe)
                            }
                        }

                        val adapter = context?.let {
                            CafeAdapter(
                                it,
                                R.layout.layout_list_cafe, cafeList)
                        }
                        listData.adapter = adapter
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }
            })
    }
}